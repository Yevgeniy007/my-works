"use strict";

const burger = document.querySelector(".header__button-burger");
const navMenu = document.querySelector(".header__list-mobile");
const body = document.querySelector("body");

burger.addEventListener("click", (e) => {
    if (burger.classList.contains("header__button-cancel")) {
        e.target.classList.remove("header__button-cancel");
        navMenu.style.display = "none";
        e.stopPropagation();
    } else {
        e.target.classList.add("header__button-cancel");
        navMenu.style.display = "block";
        e.stopPropagation()
    }
})

body.addEventListener("click", (e) => {
        if (burger.classList.contains("header__button-cancel")) {
            burger.classList.remove("header__button-cancel");
            navMenu.style.display = "none";    
        }
    })