import gulp from 'gulp';
import dartSass from 'sass';
import gulpSass from 'gulp-sass';
import clean from 'gulp-clean';
import browserSync from 'browser-sync';
import concat from 'gulp-concat';
import terser from 'gulp-terser';
import htmlMin from 'gulp-htmlmin';
import autoprefixer from 'gulp-autoprefixer';
import imagemin from 'gulp-imagemin';

const BS = browserSync.create();
const sass = gulpSass(dartSass);

const cleanDist = () => gulp.src('dist/*', { allowEmpty: true })
    .pipe(clean());

const concatJs = () => gulp.src("./src/scripts/**/*.js")
    .pipe(concat("scripts.min.js"))
    .pipe(terser())
    .pipe(gulp.dest("./dist/skripts"));

const buildStyles = () => gulp.src('./src/styles/**/*.scss')
    .pipe(autoprefixer())
    .pipe(concat('styles.min.css'))
    .pipe(sass())
    .pipe(gulp.dest('./dist/css'));

const moveImages = () => gulp.src('./src/images/**/*')
    .pipe(imagemin())
    .pipe(gulp.dest('./dist/images'));

const minifyHtml = () => gulp.src('./**/*.html')
    .pipe(htmlMin({collapseWhitespace: true}))
    .pipe(gulp.dest('./dist'));

export const build = gulp.series(cleanDist, minifyHtml, concatJs, moveImages, buildStyles);

export const dev = gulp.series(build,  () => {
    BS.init({
        server: {
            baseDir: "./"
        }
    });

    gulp.watch('./src/**/*', gulp.series(concatJs, buildStyles, (done) => {
        BS.reload();
        done();
    }))
});