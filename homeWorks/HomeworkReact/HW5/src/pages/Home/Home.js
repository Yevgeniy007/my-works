import React from 'react';
import Cards from '../../components/Cards/Cards';
import { useSelector } from 'react-redux';

const Home = () => {

	const products = useSelector(store => store.products.products);

	return (
		<section className="container">
			<h2 className='section__title'>Каталог товарів</h2>
			<Cards products={products}/>
		</section>
	);
}

export default Home;