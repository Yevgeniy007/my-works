import React from 'react';
import PropTypes from 'prop-types';
import styles from './ProductsModal.module.scss';
import ProductsItem from '../ProductsItem/ProductsItem';

const ProductsModal = ({ products, handleDeleteFromList }) => {
	return (
		<div className={styles.wrapper}>
			<ul className={styles.list}>
				{products.map(product => (
					<ProductsItem key={product.id} product={product} handleDelete={handleDeleteFromList} />
				))}
			</ul>
		</div>
	);
}

ProductsModal.propTypes = {
	products: PropTypes.arrayOf(PropTypes.object),
}

export default ProductsModal;
