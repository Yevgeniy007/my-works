import React, { useEffect, useState } from 'react';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import CustomField from '../CustomField/CustomField';
import CustomNumberField from '../CustomNumberField/CustomNumberField';
import styles from './CheckoutForm.module.scss';
import ProductsItem from '../ProductsItem/ProductsItem';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { setModalData, setModalIsOpen } from '../../store/modal/actionCreators';
import { toggleProductInCart, clearCart} from '../../store/products/actionCreators';
import { createOrder } from '../../store/order/actionCreators';

const CheckoutForm = ({ products }) => {
	const [totalPrice, setTotalPrice] = useState(0);

	useEffect(() => {
		const calculateTotalPrice = () => {
			if (products.length > 0) {
				return products.reduce((accum, current) => accum + current.price, 0);
			}
			return 0;
		};

		setTotalPrice(calculateTotalPrice);
	}, [products]);

	const dispatch = useDispatch();

	const handleProductDelete = id => {
		const title = products.find(product => product.id === id).title;

		dispatch(setModalIsOpen(true));
		dispatch(
			setModalData({
				header: 'Видалити товар з кошика?',
				text: `Назва товару: ${title}`,
				handleSubmit: () => {
					dispatch(toggleProductInCart(id));
					dispatch(setModalIsOpen(false));
				},
			})
		);
	};

	const initialValues = {
		name: '',
		surname: '',
		age: '',
		address: '',
		phoneNumber: '',
	};

	const validationSchema = Yup.object().shape({
		name: Yup.string().required("Поле обов'язкове до заповнення:"),
		surname: Yup.string().required("Поле обов'язкове до заповнення:"),
		age: Yup.number()
			.min(16, 'Вам повинно бути більше 16')
			.max(1500, 'Ваш вік вірний?')
			.required("Поле обов'язкове до заповнення:"),
		address: Yup.string().required("Поле обов'язкове до заповнення:"),
		phoneNumber: Yup.string()
			.matches(
				/\+38\([0-9]{3}\)[0-9]{3}-[0-9]{2}-[0-9]{2}/,
				'Введіть номер телефону:'
			)
			.required("Поле обов'язкове до заповнення:"),
	});

	const onSubmit = (values, { resetForm }) => {
		dispatch(clearCart());
		resetForm();
		dispatch(createOrder({ total: totalPrice, userInfo: values, products }));
	};

	return (
		<Formik
			initialValues={initialValues}
			validationSchema={validationSchema}
			onSubmit={onSubmit}
		>
			{({ isValid }) => (
				<Form>
					<div className={styles.fields}>
						<h3>Заповніть Ваші дані:</h3>
						<CustomField className={styles.field} label="Ім'я" name="name" />
						<CustomField className={styles.field} label="Прізвище:" name="surname" />
						<CustomField className={styles.field} label="Вік" name="age" />
						<CustomField className={styles.field} label="Адреса доставки:" name="address" />
						<CustomNumberField className={styles.field} label="Номер телефону:" name="phoneNumber" /> 
					</div>
					<div>
						<h3>Ваше замовлення</h3>
						<ul className={styles.order}>
							{products.map(product => (
								<ProductsItem key={product.id} product={product} handleDelete={() => handleProductDelete(product.id)} />
							))}
						</ul>
					</div>
					<footer className={styles.footer}>
						<span className={styles.total}>
							<span>Всього: </span>
							{totalPrice}грн.
						</span>
						<button
							type="submit"
							disabled={!isValid}
							className={styles.checkoutBtn}
						>
							Checkout
						</button>
					</footer>
				</Form>
			)}
		</Formik>
	);
};

CheckoutForm.propTypes = {
	products: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default CheckoutForm;
