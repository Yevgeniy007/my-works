import React from 'react';
import styles from './ProductsItem.module.scss';
import PropTypes from 'prop-types';

const ProductsModalItem = ({ product: { id, title, price, image }, handleDelete }) => {
	return (
		<li className={styles.item}>
			<img className={styles.img} src={image} alt={title} />
			<span className={styles.title}>{title}</span>
			<span className={styles.price}>{price}</span>
			<button type='button' className={styles.deleteButton} onClick={() => { handleDelete(id) }}>
				<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
					viewBox="0 0 50 50"
					fill="currentColor">
					<path d="M 7.71875 6.28125 L 6.28125 7.71875 L 23.5625 25 L 6.28125 42.28125 L 7.71875 43.71875 L 25 26.4375 L 42.28125 43.71875 L 43.71875 42.28125 L 26.4375 25 L 43.71875 7.71875 L 42.28125 6.28125 L 25 23.5625 Z"></path>
				</svg>
			</button>
		</li>
	);
}

ProductsModalItem.propTypes = {
	product: PropTypes.shape({
		id: PropTypes.number.isRequired,
		title: PropTypes.string,
		price: PropTypes.number,
		image: PropTypes.string,
		color: PropTypes.string,
		isFavourite: PropTypes.bool,
		isInCart: PropTypes.bool,
	}).isRequired,
	handleDelete: PropTypes.func.isRequired,
}

export default ProductsModalItem;
