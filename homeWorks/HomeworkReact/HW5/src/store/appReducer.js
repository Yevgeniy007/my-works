import { combineReducers } from "redux";
import productsReducer from "./products/productsReducer";
import modalReducer from "./modal/modalReducer";
import orderReducer from './order/orderReducer';

const appReducer = combineReducers({
	products: productsReducer,
	modal: modalReducer,
	order: orderReducer
});

export default appReducer;