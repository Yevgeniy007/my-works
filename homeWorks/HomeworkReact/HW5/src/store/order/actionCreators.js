import { CREATE_ORDER } from './actions';

export const createOrder = order => ({ type: CREATE_ORDER, payload: order });
