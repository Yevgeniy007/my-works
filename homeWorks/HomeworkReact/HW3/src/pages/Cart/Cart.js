import Cards from '../../components/Cards/Cards';

const Cart = ({ products, toggleProductToFavourites, toggleProductToCart }) => {

	return (
		<>
			<section className='container'>
				<h1 className='section__title'>Товари у кошику</h1>
				<Cards
					cards={products}
					toggleProductToFavourites={toggleProductToFavourites}
					toggleProductToCart={toggleProductToCart}
				/>
			</section>
		</>
	);
}

export default Cart;
