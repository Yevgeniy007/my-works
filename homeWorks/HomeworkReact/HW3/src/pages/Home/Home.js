import React from 'react';
import Cards from '../../components/Cards/Cards';

const Home = ({ products, toggleProductToFavourites, toggleProductToCart }) => {
	return (
		<section className="container">
			<h2 className='section__title'>Каталог товарів</h2>
			<Cards
				cards={products}
				toggleProductToFavourites={toggleProductToFavourites}
				toggleProductToCart={toggleProductToCart}
			/>
		</section>
	);
}

export default Home;