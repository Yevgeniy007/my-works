import React from 'react';
import Header from '../components/Header/Header';
import '../App.scss';
import { Outlet } from 'react-router-dom';

const PageLayout = ({ productsInCart, productsInFavourite, toggleProductToFavourites, toggleProductToCart }) => {
	return (
		<>
			<Header
				productsInCart={productsInCart}
				productsInFavourites={productsInFavourite}
				handleDeleteActions={{ handleFavourites: toggleProductToFavourites, handleCart: toggleProductToCart }} />
			<main className='main'>
				<Outlet />
			</main>
		</>
	);
}

export default PageLayout;
