import React from 'react';
import Card from '../Card/Card';
import styles from './Cards.module.scss';

const Cards = ({ cards, toggleProductToFavourites, toggleProductToCart }) => {
	return (
		<ul className={styles.cards}>
			{cards.map(product => (
				<li key={product.id}>
					<Card
						isFavourite={product.isFavourite}
						card={product}
						onAddToFavourites={toggleProductToFavourites}
						onAddToCart={toggleProductToCart}
					/>
				</li>
			))}
		</ul>
	);
}

export default Cards;