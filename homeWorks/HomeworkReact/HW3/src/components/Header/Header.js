import React from 'react';
import { NavLink } from 'react-router-dom';
import Cart from '../Cart/Cart';
import Favourites from '../Favourites/Favourites';
import styles from './Header.module.scss';

const Header = ({ productsInCart, productsInFavourites, handleDeleteActions: { handleFavourites, handleCart } }) => {
	 
	return (
		<header className={styles.header}>
			<div className={styles.inner}>
				<span className={styles.title}>Дніпро-М</span>
				<ul className={styles.pages}>
					<li>
						<NavLink
							className={({ isActive }) => isActive ? styles.current : ''}
							end
							to='/'>
							Головна
						</NavLink>
					</li>
					<li>
						<NavLink
							className={({ isActive }) => isActive ? styles.current : ''}
							to='/favoutires'>
							Список бажань
						</NavLink>
					</li>
					<li>
						<NavLink
							className={({ isActive }) => isActive ? styles.current : ''}
							to='/cart'>
							Кошик
						</NavLink>
					</li>
				</ul>
				<div className={styles.counters}>
					<Favourites productsInFavourites={productsInFavourites} handleDeleteFromList={handleFavourites} />
					<Cart productsInCart={productsInCart} handleDeleteFromList={handleCart} />
				</div>
			</div>
		</header>
	);
}

export default Header;