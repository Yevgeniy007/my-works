import React, { useState, useEffect, useRef } from 'react';
import styles from './Favourites.module.scss';
import PropTypes from 'prop-types';
import ProductsModal from '../ProductsModal/ProductsModal';

const Favourites = ({ productsInFavourites, handleDeleteFromList }) => {
	const [isInfoOpen, setIsInfoOpen] = useState(false);

	const infoModal = useRef(null);
	const favouritesBtn = useRef(null);

	useEffect(() => {
		const checkIfClickedOutside = e => {
			if (!favouritesBtn.current.contains(e.target) && isInfoOpen && infoModal.current && !infoModal.current.contains(e.target)) {
				setIsInfoOpen(false);
			}
		}

		document.addEventListener('mousedown', checkIfClickedOutside);

		if (!productsInFavourites.length) setIsInfoOpen(false);

		return () => document.removeEventListener('mousedown', checkIfClickedOutside);
	}, [isInfoOpen, productsInFavourites.length]);


	return (
		<div className={styles.holder}>
			<button
				ref={favouritesBtn}
				type='button'
				className={styles.favouritesButton}
				data-products-in-favourites={productsInFavourites.length}
				onClick={() => {
					if (productsInFavourites.length) setIsInfoOpen(prev => !prev);
				}}
			>
				<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor">
					<path strokeLinecap="round" strokeLinejoin="round" d="M11.48 3.499a.562.562 0 011.04 0l2.125 5.111a.563.563 0 00.475.345l5.518.442c.499.04.701.663.321.988l-4.204 3.602a.563.563 0 00-.182.557l1.285 5.385a.562.562 0 01-.84.61l-4.725-2.885a.563.563 0 00-.586 0L6.982 20.54a.562.562 0 01-.84-.61l1.285-5.386a.562.562 0 00-.182-.557l-4.204-3.602a.563.563 0 01.321-.988l5.518-.442a.563.563 0 00.475-.345L11.48 3.5z" />
				</svg>
			</button>
			{isInfoOpen &&
				<div ref={infoModal} className={styles.infoModal}>
					<ProductsModal products={productsInFavourites} handleDeleteFromList={handleDeleteFromList} />
				</div>}
		</div>
	);
}

Favourites.propTypes = {
	productsInFavourites: PropTypes.arrayOf(PropTypes.object).isRequired,
}

export default Favourites;