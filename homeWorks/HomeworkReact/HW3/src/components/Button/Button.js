import React from 'react';
import styles from './Button.module.scss';
import PropTypes from 'prop-types';

const Button = ({ backgroundColor, text, onClick }) => (
	<button
		className={styles.button}
		type='button'
		style={{ backgroundColor }}
		onClick={onClick}
	>
		{text}
	</button>
)

Button.propTypes = {
	backgroundColor: PropTypes.string,
	text: PropTypes.string,
	onClick: PropTypes.func.isRequired,
}

Button.defaultProps = {
	backgroundColor: '',
	text: '',
}

export default Button;
