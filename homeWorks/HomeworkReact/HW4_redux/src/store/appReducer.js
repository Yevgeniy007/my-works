import { combineReducers } from "redux";
import productsReducer from "./products/productsReducer";
import modalReducer from "./modal/modalReducer";

const appReducer = combineReducers({
	products: productsReducer,
	modal: modalReducer,
});

export default appReducer;