import React from 'react';
import Cards from '../../components/Cards/Cards';
import { useSelector, shallowEqual } from 'react-redux';

const Favourites = () => {

	const productsInFavourites = useSelector(store => store.products.productsInFavourites, shallowEqual);

	return (
		<section className='container'>
			<h1 className='section__title'>Список бажань</h1>
			<Cards products={productsInFavourites} />
		</section>
	);
}

export default Favourites;
