import Cards from '../../components/Cards/Cards';
import { useSelector, shallowEqual } from 'react-redux';


const Cart = () => {

	const productsInCart = useSelector(store => store.products.productsInCart, shallowEqual);
	
	return (
		<>
			<section className='container'>
				<h1 className='section__title'>Товари у кошику</h1>
				<Cards products={productsInCart} />
			</section>
		</>
	);
}

export default Cart;
