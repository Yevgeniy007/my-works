import React, { Component } from 'react';
import Card from '../Card/Card';
import styles from './Cards.module.scss';

class Cards extends Component {
	render() {
		const { cards, toggleProductToFavourites, toggleProductToCart } = this.props;
		return (
			<ul className={styles.cards}>
				{cards.map(product => (
					<li key={product.id}>
						<Card
							card={product}
							onAddToFavourites={toggleProductToFavourites}
							onAddToCart={toggleProductToCart}
						/>
					</li>
				))}
			</ul>
		);
	}
}

export default Cards;