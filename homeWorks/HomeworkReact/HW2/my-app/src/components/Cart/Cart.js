import React, { Component } from 'react';
import styles from './Cart.module.scss';
import PropTypes from 'prop-types';
import ProductsModal from '../ProductsModal/ProductsModal';

class Cart extends Component {
	static propTypes = {
		productsInChart: PropTypes.arrayOf(PropTypes.object).isRequired,
	}

	state = {
		isInfoOpen: false,
	}

	infoModal = React.createRef();
	favouritesBtn = React.createRef();

	checkIfClickedOutside = e => {
		if (!this.favouritesBtn.current.contains(e.target) && this.state.isInfoOpen && this.infoModal.current && !this.infoModal.current.contains(e.target)) {
			this.setState({ isInfoOpen: false });
		}
	}

	componentWillUnmount() {
		document.removeEventListener('mousedown', this.checkIfClickedOutside);
	}

	render() {
		const { productsInChart, handleDeleteFromList } = this.props;
		const { isInfoOpen } = this.state;
		return (
			<div className={styles.holder}>
				<button
					ref={this.favouritesBtn}
					type='button'
					className={styles.cartButton}
					data-products-in-cart={productsInChart.length}
					onClick={() => {
						if (productsInChart.length) this.setState({ isInfoOpen: !isInfoOpen });
						document.addEventListener('mousedown', this.checkIfClickedOutside);
					}}>
					<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor">
						<path strokeLinecap="round" strokeLinejoin="round" d="M2.25 3h1.386c.51 0 .955.343 1.087.835l.383 1.437M7.5 14.25a3 3 0 00-3 3h15.75m-12.75-3h11.218c1.121-2.3 2.1-4.684 2.924-7.138a60.114 60.114 0 00-16.536-1.84M7.5 14.25L5.106 5.272M6 20.25a.75.75 0 11-1.5 0 .75.75 0 011.5 0zm12.75 0a.75.75 0 11-1.5 0 .75.75 0 011.5 0z" />
					</svg>
				</button>
				{isInfoOpen && productsInChart.length !== 0 &&
					<div ref={this.infoModal} className={styles.infoModal}>
						<ProductsModal products={productsInChart} handleDeleteFromList={handleDeleteFromList} />
					</div>}
			</div>
		);
	}
}

export default Cart;
