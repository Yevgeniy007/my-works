import React, { PureComponent } from 'react';
import Cart from '../Cart/Cart';
import Favourites from '../Favourites/Favourites';
import styles from './Header.module.scss';
// import PropTypes from 'prop-types';

class Header extends PureComponent {

	render() {
		const { productsInChart, productsInFavourites, handleDeleteActions: { handleFavourites, handleCart } } = this.props;
		return (
			<header className={styles.header}>
				<div className={styles.inner}>
					<span className={styles.title}>Дніпро-М</span>
					<div className={styles.counters}>
						<Favourites productsInFavourites={productsInFavourites} handleDeleteFromList={handleFavourites} />
						<Cart productsInChart={productsInChart} handleDeleteFromList={handleCart} />
					</div>
				</div>
			</header>
		);
	}
}

export default Header;