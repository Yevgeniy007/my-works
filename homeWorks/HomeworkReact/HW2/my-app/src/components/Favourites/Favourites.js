import React, { Component } from 'react';
import styles from './Favourites.module.scss';
import PropTypes from 'prop-types';
import ProductsModal from '../ProductsModal/ProductsModal';

class Favourites extends Component {
	static propTypes = {
		productsInFavourites: PropTypes.arrayOf(PropTypes.object).isRequired,
	}

	state = {
		isInfoOpen: false,
	}

	infoModal = React.createRef();
	favouritesBtn = React.createRef();

	checkIfClickedOutside = e => {
		if (!this.favouritesBtn.current.contains(e.target) && this.state.isInfoOpen && this.infoModal.current && !this.infoModal.current.contains(e.target)) {
			this.setState({ isInfoOpen: false });
		}
	}

	componentWillUnmount() {
		document.removeEventListener('mousedown', this.checkIfClickedOutside);
	}

	render() {
		const { productsInFavourites, handleDeleteFromList } = this.props;
		const { isInfoOpen } = this.state;
		return (
			<div className={styles.holder}>
				<button
					ref={this.favouritesBtn}
					type='button'
					className={styles.favouritesButton}
					data-products-in-favourites={productsInFavourites.length}
					onClick={() => {
						if (productsInFavourites.length) this.setState({ isInfoOpen: !isInfoOpen });

						document.addEventListener('mousedown', this.checkIfClickedOutside);
					}}
				>
					<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor">
						<path strokeLinecap="round" strokeLinejoin="round" d="M11.48 3.499a.562.562 0 011.04 0l2.125 5.111a.563.563 0 00.475.345l5.518.442c.499.04.701.663.321.988l-4.204 3.602a.563.563 0 00-.182.557l1.285 5.385a.562.562 0 01-.84.61l-4.725-2.885a.563.563 0 00-.586 0L6.982 20.54a.562.562 0 01-.84-.61l1.285-5.386a.562.562 0 00-.182-.557l-4.204-3.602a.563.563 0 01.321-.988l5.518-.442a.563.563 0 00.475-.345L11.48 3.5z" />
					</svg>
				</button>
				{isInfoOpen && productsInFavourites.length !== 0 &&
					<div ref={this.infoModal} className={styles.infoModal}>
						<ProductsModal products={productsInFavourites} handleDeleteFromList={handleDeleteFromList} />
					</div>}
			</div>
		);
	}
}

export default Favourites;