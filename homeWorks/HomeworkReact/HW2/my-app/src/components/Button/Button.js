import React, { PureComponent } from 'react';
import styles from './Button.module.scss';

class Button extends PureComponent {
	render() {
		const { onClick, backgroundColor, text } = this.props;
		return (
			<button
				className={styles.button}
				onClick={onClick}
				type='button'
				style={{ backgroundColor }}
			> 
			{text}
			</button>
		);
	}
}

export default Button;