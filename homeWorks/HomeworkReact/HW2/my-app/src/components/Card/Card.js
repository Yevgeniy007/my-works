import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './Card.module.scss';
import Modal from '../Modal/Modal';
import Button from '../Button/Button';

class Card extends Component {
	static propTypes = {
		card: PropTypes.shape({
			id: PropTypes.number.isRequired,
			title: PropTypes.string,
			price: PropTypes.number,
			image: PropTypes.string,
			color: PropTypes.string,
			isFavourite: PropTypes.bool,
			isInCart: PropTypes.bool,
		}),
		onAddToCart: PropTypes.func.isRequired,
		onAddToFavourites: PropTypes.func.isRequired,
	}

	static defaultProps = {
		card: {
			id: 0,
			title: '',
			price: 0,
			image: '',
			color: '',
			isFavourite: false,
			isInCart: false,
		},
	}

	state = {
		hasRequestedToAddToCart: false,
	}

	closeModal = () => {
		this.setState({ hasRequestedToAddToCart: false })
	}

	render() {
		const { card: { title, image, price, color, id, isFavourite, isInCart }, onAddToCart, onAddToFavourites, } = this.props;
		const { hasRequestedToAddToCart } = this.state;
		return (
			<>
				<article className={styles.card}>
					<button type='button' className={styles.addToFavouriteButton} onClick={() => {
						onAddToFavourites(id, isFavourite);
					}}>
						<svg xmlns="http://www.w3.org/2000/svg" fill={isFavourite ? "currentColor" : "none"} viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor">
							<path strokeLinecap="round" strokeLinejoin="round" d="M11.48 3.499a.562.562 0 011.04 0l2.125 5.111a.563.563 0 00.475.345l5.518.442c.499.04.701.663.321.988l-4.204 3.602a.563.563 0 00-.182.557l1.285 5.385a.562.562 0 01-.84.61l-4.725-2.885a.563.563 0 00-.586 0L6.982 20.54a.562.562 0 01-.84-.61l1.285-5.386a.562.562 0 00-.182-.557l-4.204-3.602a.563.563 0 01.321-.988l5.518-.442a.563.563 0 00.475-.345L11.48 3.5z" />
						</svg>
					</button>
					<div className={styles.imgContainer}>
						<img src={image} alt={title} />
					</div>
					<div className={styles.mainContent}>
						<h6 className={styles.title}>{title}</h6>
						<span className={styles.color}><span>Колір:</span> {color}</span>
						<span className={styles.id}>Артикул: {id}</span>
					</div>
					<footer className={styles.footer}>
						<span className={styles.price}>{price}</span>
						<button
							className={styles.addToCartButton}
							onClick={() => this.setState({ hasRequestedToAddToCart: true })}>
							{isInCart ? 'У кошику' : 'Додати до кошика'}
						</button>
					</footer>
				</article >
				{
					hasRequestedToAddToCart &&
					<Modal
						header={!isInCart ? 'Додати товар до кошика?' : 'Видалити товар з кошика?'}
						closeButton
						handleOnClose={this.closeModal}
						actions={
							<div>
								<Button text='Скасувати' backgroundColor={"black"} onClick={() => {
									this.closeModal();
								}} />
								<Button text={!isInCart ? 'Додати' : 'Видалити'}  backgroundColor={"black"} onClick={() => {
									onAddToCart(id, isInCart);
									this.closeModal();
								}} />
							</div>
						}
					/>
				}
			</>
		);
	}
}

export default Card;

