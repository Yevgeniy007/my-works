"use strict"

const changeTheme = document.querySelector(".btn");
const textColor = document.querySelector(".color-text");
const head = document.querySelector("head");

if (localStorage.getItem('themeIsBlack') === null) {
    localStorage.setItem('themeIsBlack', 'true')
}

changeTheme.addEventListener("click", () => {
    console.log(localStorage.getItem('themeIsBlack'));

    if (localStorage.getItem('themeIsBlack') === 'true') {
        head.insertAdjacentHTML('beforeend', '<link class ="white-theme" rel="stylesheet" href="./stileWhiteTheme.css">');
        localStorage.setItem('themeIsBlack', 'false')
    } else {
        document.querySelector(".white-theme").remove()
        localStorage.setItem('themeIsBlack', 'true')
    }
})

if (localStorage.getItem('themeIsBlack') === 'false') {
    head.insertAdjacentHTML('beforeend', '<link class ="white-theme" rel="stylesheet" href="./stileWhiteTheme.css">');
} else {
    document.querySelector(".white-theme").remove();
}