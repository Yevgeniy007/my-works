// 1. Опишіть своїми словами як працює метод forEach: 
//  - виконує callback функцію для кожного елемента масиву;
// 2. Як очистити масив?
//  - arr.splice(0,arr.length);
//  - arr.length=0;
//  - arr = [];
// 3. Як можна перевірити, що та чи інша змінна є масивом?
//  - метод Array.isArray() повертає true, якщо объект є масивом, інакше false.



const array = ['hello', 'world', 0, 23, '23', null, {}, undefined, false];

const filterBy = (arr, type) => arr.filter(function (element) {
    if (type === "null" && element === null ) {
        return false;
    } else if (type === "object" && element === null) {
        return true;
    } return typeof element !== type;
})


const forEachBy = function (arr, type) {
    const ArrayFilterForEach = [];
    arr.forEach(function (elem) {
        if (typeof elem !== type && type !== "null" || type === "null" && elem !== null || type === "object" && elem === null) {
            ArrayFilterForEach.push(elem);
        }
    })
    return ArrayFilterForEach;
}


console.log(array);

const allTypes = ['string', 'number', 'boolean', 'null', 'undefined', 'object'];

allTypes.forEach(type => console.log(filterBy(array, type)));

allTypes.forEach(type => console.log(forEachBy(array, type)));
