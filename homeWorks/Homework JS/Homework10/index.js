document.querySelectorAll('.centered-content li').forEach(li => {
    li.addEventListener('click', (event) => {
        document.querySelector('.active')?.classList.remove('active');
        event.target.classList.add('active');

        document.querySelector('.active-text')?.classList.remove('active-text');
        document.querySelectorAll('.tabs-content li').forEach(li => {
            if (li.dataset.index === event.target.dataset.index) {
                li.classList.add('active-text');
            }
        })
    })
})
