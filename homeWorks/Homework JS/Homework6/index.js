// 1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування:
// - це обернений слеш для відображення спецсимволів;
// 2. Які засоби оголошення функцій ви знаєте?
// - function name(arguments){};
// - const name = function(arguments){};
// - () => {};
// 3. Що таке hoisting, як він працює для змінних та функцій?
// - це підняття, оголошення змінних та функцій розміщуються в пам'ять на етапі компіляції, але вони залишаються саме там, де ми ввели їх у свій код.

//                         ЗАВДАННЯ 1
const validate = (value) => value === null || value === "";

function validateDate(value) {
    const arrDate = value.split(".");
    arrDate[1] -= 1;
    const date = new Date(arrDate[2], arrDate[1], arrDate[0]);

    return !((date.getFullYear() == arrDate[2]) && (date.getMonth() == arrDate[1]) && (date.getDate() == arrDate[0]));
}

function getUserValue(massage = "Enter value: ", valid) {
    let userVal = null;
    do {
        userVal = prompt(massage);
    } while (valid(userVal));
    return userVal;
}

const formatateDate = (data) => `${data.slice(6, 10)},${data.slice(3, 5)},${data.slice(0, 2)}`;

const createNewUser = () => {
    return {
        firstName: getUserValue('Enter first name', validate),
        lastName: getUserValue('Enter last name', validate),
        birthday: new Date(formatateDate(getUserValue("Enter date of birth:  dd.mm.yyyy", validateDate))),
        getLogin() {
            return ((this.firstName[0] + this.lastName).toLowerCase());
        },
        getAge() {
            const today = new Date();
            if (today.getMonth() > this.birthday.getMonth() || today.getMonth() === this.birthday.getMonth() && today.getDate() >= this.birthday.getDate()) {
                return today.getFullYear() - this.birthday.getFullYear();
            }
            return today.getFullYear() - this.birthday.getFullYear() - 1;
        },
        getPassword() {
            return this.firstName.slice(0, 1).toUpperCase() + this.lastName.toLowerCase() + new Date(this.birthday).getFullYear();
        },
    }
}

let myUser = createNewUser();

Object.defineProperties(myUser, {
    firstName: {
        writable: false,
        configurable: true
    },
    lastName: {
        writable: false,
        configurable: true,
    }
});

myUser.setNewFirstName = function (newFName) {
    Object.defineProperty(myUser, 'firstName', {
        value: newFName
    })
}
myUser.setNewLastName = function (newLName) {
    Object.defineProperty(myUser, 'lastName', {
        value: newLName
    })
}

console.log(myUser);

console.log(myUser.getAge());

console.log(myUser.getPassword());