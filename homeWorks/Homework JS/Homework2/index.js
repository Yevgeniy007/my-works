// 1. Які існують типи даних у Javascript?
//     - number - для цілих чисел;
//     - bigint - для чисел великої довжини;
//     - string - для строк;
//     - Boolean - для true/false;
//     - null - для невідомих значень;
//     - undefined - для неприсвоєних значень;
//     - Object - для складних структур даних;
//     - Symbol - для унікальних ідентифікаторів.

// 2. У чому різниця між == і ===?
//     == не строге порівняння, а === строге порівняння без приведення типу даних.

// 3. Що таке оператор?
//     - це символи, знаки та їхні поєднані конструкції, оператори виконують операції над визначеними значеннями, які називаються операндами. Самі оператори представлені одним чи кількома символами, буває словом. Більшість операторів відповідають математичним операціям.

//                               Задача

let nameUser = prompt("What's your name?", ["name"]);
while (nameUser === "" || nameUser === "name") {
    nameUser = prompt("What's your name?", ["name"]);
}

let ageUser = +prompt("What is your age?", ["age"]);
while (isNaN(ageUser)) {
    ageUser = +prompt("What is your age?", ["age"]);
}

if (ageUser < 18) {
    alert("You are not allowed to visit this website");
} else if (ageUser >= 18 && ageUser <= 22) {
    if (confirm("Are you sure you want to continue?")) {
        alert("Welcome, " + nameUser + "!")
    } else {
        alert("You are not allowed to visit this website");
    }
} else {
    alert("Welcome, " + nameUser + "!")
}
