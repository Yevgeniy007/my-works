// Чому для роботи з input не рекомендується використовувати клавіатуру?
// - тому що різні обробники зчитують по різному з різних операційних систем клавіши і це може призвести до вводу некоректної інформації в input;
// - Деякі мобільні пристрої також не генерують keypress/keydown, а одразу вставляють текст у поле. Обробити введення на них за допомогою клавіатурних подій не можна

"use strict"

const btnCollection = document.querySelectorAll('.btn')

const highlightKey = function (collection) {
    document.addEventListener('keyup', (event) => {
        collection.forEach(btn => {
            if (btn.dataset.value === event.code) {
                document.querySelector('.btn-blue')?.classList.remove('btn-blue');
                btn.classList.add('btn-blue');
            }
        })
    })
}

highlightKey(btnCollection);