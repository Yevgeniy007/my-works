//  1. Описати своїми словами навіщо потрібні функції у програмуванні:
//     - щоб не писати багато разів повторний код, а записати один раз та визивати з потрібними аргументами в потрібному міці багато разів;
//  2. Описати своїми словами, навіщо у функцію передавати аргумент:
//     - по суті функція це узагальнений набір дій по роботі над аргументами, для повернення нам результату роботи конкретної функції над       конкретними даними (аргументами);
//  3. Що таке оператор return та як він працює всередині функції:
//     - return - завершує виконання поточної функції та повертає її значення.


//                         ЗАВДАННЯ 1


const validateIsNumber = (userValue) => isNaN(+userValue) || userValue === null || userValue === "";
const validateIsOperation = (userValue) => !(userValue === "+" || userValue === "-" || userValue === "*" || userValue === "/");

function getUserValue(massage = "Enter value: ", valid) {
    let userVal = null;
    do {
        userVal = prompt(massage, [userVal]);
    } while (valid(userVal));
    return userVal;
}

function calculator() {
    const firstNumber = +getUserValue("Enter first number:", validateIsNumber);
    const secondNumber = +getUserValue("Enter second number:", validateIsNumber);
    const mathOperation = getUserValue("Enter math operation", validateIsOperation);

    switch (mathOperation) {
        case '-':
            return firstNumber - secondNumber;
        case '+':
            return firstNumber + secondNumber;
        case '*':
            return firstNumber * secondNumber;
        case '/':
            return firstNumber / secondNumber;
        default:
            alert("There is no such operator");
    }
}

console.log(calculator());