// 1. Опишіть своїми словами різницю між функціями setTimeout() і setInterval():
//  - setTimeout() дозволяє викликати функцію один раз через певний проміжок часу, а setInterval() дозволяє викликати функцію регулярно, повторюючи виклик через певний проміжок часу.
// 2. Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
//  - не спрацює миттєво, тому що вона вийде з потоку і буде чекати своєї черги в потік!
// 3. Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
//  - тому ще setInterval() запускає безкінечний цикл і його потрібно зупиняти щоб не загружати потік!

"use strict"

const slides = document.querySelectorAll('.image-to-show');
const stopShowSlides = document.querySelector(".stop-show");
const goShowSlides = document.querySelector(".go-show");
let slideIndex = 0;



const fadeIn = (el, timeout, display) => {
    el.style.opacity = 0;
    el.style.display = display;
    el.style.transition = `opacity ${timeout}ms`;
    setTimeout(() => {
        el.style.opacity = 1;
    }, 10);
};

function showSlides(n) {
    if (n >= slides.length) {
        slideIndex = 0;
    }
    for (let slide of slides) {
        slide.style.display = "none";
    }
    fadeIn(slides[slideIndex], 1000, "block");
}4

function countDownTimerMlSec(mlSec) {
    const timer = setInterval(function () {
        const timerMlSecSpan = document.getElementById('safeTimerDisplayMlSec');
        timerMlSecSpan.innerHTML = mlSec;
        mlSec -= 4;
        if (mlSec <= 0) {
            clearInterval(timer);
            timerMlSecSpan.innerHTML = "";
        } return;
    }, 1);
}

showSlides(slideIndex);
countDownTimerMlSec(3000);

let timerId = setInterval(() => {
    countDownTimerMlSec(3000);
    showSlides(slideIndex += 1);
}, 3100);

stopShowSlides.addEventListener("click", () => {
    clearInterval(timerId);
});

goShowSlides.addEventListener("click", () => {
    timerId = setInterval(() => {
        countDownTimerMlSec(3000);
        showSlides(slideIndex += 1);
    }, 3100);
});






