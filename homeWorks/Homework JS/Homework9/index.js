// 1. Опишіть, як можна створити новий HTML тег на сторінці:
//  - document.append.(createElement('element'))  та document.body.innerHTML = 'element'

// 2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра:
//  - визначає позицію добавляємого элемента відносно элемента, який визиває цей метод:
//      - "beforebegin" - перед самим елементом.
//      - "afterbegin" - всередині елемента, на початку.
//      - "beforeend" - всередині елемента, в кінці.
//      - "afterend" - після самого елементу.

// 3. Як можна видалити елемент зі сторінки?
//  - document.querySelector(element).remove()  та  document.querySelector(element).innerHTML = ''

"use strict"

const array = ['hello', 'world', 0, 23, [7, 7, [1, 1, 1, 1, 1],7, 7, 7],  14, 29, 'hhgfhjh', 'gvfdfghse'];

const showArrayInList = function (arr, container = document.body) {

    const newUl = document.createElement("ul");
    container.append(newUl);

    arr.map((elem) => {
        if (Array.isArray(elem)){
            return showArrayInList(elem, container.querySelector('ul'));  
            }  
        newUl.insertAdjacentHTML("beforeend", `<li>${elem}</li>`) 
    })
}

// Такой вариант как по заданию тоже работает но мне не нравиться что map сначала полностью отрабатывает а потом insertAdjacentHTML все вставляет одной строкой  и вложенные списки располагаются вверху страници! или так и нада?
//
// const showArrayInList = function (arr, container = document.body) {
//     const newUl = document.createElement("ul");

//     container.appendChild(newUl);

//     newUl.insertAdjacentHTML("beforeend", `${arr.map((elem, i) => {
//         if (Array.isArray(elem)){
//                 showArrayInList(elem, document.querySelector('ul'));
//                 return;
//             }
//         return arr[i] = `<li>${elem}</li>` 
//     })
//     .join(' ')}`)
// } 

function clearThePage() {
    document.body.innerHTML = '';
    }

function countDownTimer(sec) {
    const timer = setInterval(function(){
    document.getElementById('safeTimerDisplay').innerHTML=sec;
    sec--;
    if (sec <= 0) {
        clearInterval(timer);
    }  return;
}, 1000);
}

showArrayInList(array);

setTimeout(clearThePage, 3100);

countDownTimer(2);