// 1.Опишіть своїми словами що таке Document Object Model (DOM)
// - об'єктна модель документа, це деревовидне представлення веб-сайту, завантажуваного в браузер, в виді серії об'єктів, вложених один в одного.
// 2.Яка різниця між властивостями HTML-елементів innerHTML та innerText?
// -innerHTML вставляє код в HTML-документ, а innerText вставляє простий текст, який не розпізнається як код!
// 3.Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
// - document.querySelector('selector') - кращий
// - document.querySelectorAll('selector') - кращий
// - getElementById()
// - getElementsByClassName()
// - getElementsByName()
// - getElementsByTagName()



const paragrafList = document.querySelectorAll('p')
paragrafList.forEach(element => {
    element.style.backgroundColor = '#ff0000';
});

const elementForId = document.querySelector('#optionsList');
console.log(elementForId);

const parentElement = elementForId.parentElement;
console.log(parentElement);

const childElements = elementForId.children;
console.log(childElements);

document.querySelector('#testParagraph').innerHTML = "This is a paragraph";

const childclassOfMainHeader = document.querySelector('.main-header').children;
console.log(childclassOfMainHeader);
for (const elem of childclassOfMainHeader) {
    elem.classList.add('nav-item');
}

const elemClassOfSectionTitle = document.querySelectorAll('.section-title');
elemClassOfSectionTitle.forEach(element => {
    element.classList.remove('section-title')
});

