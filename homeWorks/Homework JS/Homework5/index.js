// 1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування:
// - це обернений слеш для відображення спецсимволів;
// 2. Які засоби оголошення функцій ви знаєте?
// - function name(arguments){};
// - const name = function(arguments){};
// - () => {};
// 3. Що таке hoisting, як він працює для змінних та функцій?
// - це підняття, оголошення змінних та функцій розміщуються в пам'ять на етапі компіляції, але вони залишаються саме там, де ми ввели їх у свій код.

//                         ЗАВДАННЯ 1

const validate = (value) => value === null || value === "";

function getUserValue(massage = "Enter value: ", valid) {
    let userVal = null;
    do {
        userVal = prompt(massage);
    } while (valid(userVal));
    return userVal;
}

const createNewUser = () => {
    return {
        firstName: getUserValue('Enter first name', validate),
        lastName: getUserValue('Enter last name', validate),
        getLogin() {
            return ((this.firstName[0] + this.lastName).toLowerCase());
        }
    }
}

let myUser = createNewUser();
console.log(myUser);
console.log(myUser.getLogin());



Object.defineProperties(myUser, {
    firstName: {
        writable: false,
        configurable: true
    },
    lastName: {
        writable: false,
        configurable: true,
    }
});

myUser.setNewFirstName = function (newFName) {
    Object.defineProperty(myUser, 'firstName', {
        value: newFName
    })
}
myUser.setNewLastName = function (newLName) {
    Object.defineProperty(myUser, 'lastName', {
        value: newLName
    })
}

myUser.firstName = "TEST11"; // Попытались напрямую перезаписать имя на другое. 
myUser.lastName = "TEST22"; // Попытались напрямую перезаписать фамилию на другую. 
console.log(myUser); // Не смотря на попытки перезаписать имя / фамилию -они остались старыми, так как writable: false
myUser.setNewFirstName("TEST1"); // Пытаемся перезаписать имя через метод
myUser.setNewLastName("TEST2"); // Пытаемся перезаписать фамилию через метод
console.log(myUser); // Имя и фамилия теперь новые, потому что configurable: true и через повторные вызовы Object.defineProperty они перезаписываются, а напрямую -нет. 