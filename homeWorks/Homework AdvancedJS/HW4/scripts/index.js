"use strict"

fetch('https://ajax.test-danit.com/api/swapi/films')
	.then(response => response.json())
	.then(res => {
		res.forEach(({ episodeId, name, characters, openingCrawl }) => {
			const filmsCard = document.createElement('div');
			filmsCard.classList.add('films__card');

			document.querySelector(".films__container").append(filmsCard)

			filmsCard.insertAdjacentHTML("beforeend",
				`<p class="films__episodeId"> Part : ${episodeId}</p>
					<h2 class="films__name">Title : ${name}</h2>
        			<span class="films__text">${openingCrawl}</span>`
			)

			const filmCharacters = document.createElement('ul');
			filmCharacters.classList.add('film__characters');
			filmsCard.append(filmCharacters);

			const loader = document.createElement('div');
				loader.classList.add('middle');
				loader.innerHTML = `<div class="bar bar1"></div>
				<div class="bar bar2"></div>
				<div class="bar bar3"></div>
				<div class="bar bar4"></div>
				<div class="bar bar5"></div>
				<div class="bar bar6"></div>
				<div class="bar bar7"></div>
				<div class="bar bar8"></div>`;
			filmCharacters.append(loader);

			characters.forEach(character => {
				fetch(character)
				.then(response => response.json())
				.then(({ name }) => {
					loader.remove();
					filmCharacters.insertAdjacentHTML('beforeend', `
					<li class="character">${name}</li>`)
				})
				.catch(err => console.log(err));
			})
		})
	})
	.catch ((err) => {
	document.body.innerHTML = `<h1>Error: ${err.message}</h1>`
	})




	









