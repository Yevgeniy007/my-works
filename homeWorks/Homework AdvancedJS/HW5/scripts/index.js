"use strict"

const container = document.querySelector(".post__container");

class Post {
	constructor(body, title, userId, name, mail, id) {
		this.body = body,
			this.title = title,
			this.userId = userId,
			this.name = name,
			this.mail = mail,
			this.id = id,
			this.editButton = document.createElement("button");
		this.deleteButton = document.createElement("button");
	}

	createElements() {
		container.insertAdjacentHTML('afterbegin',
			`<article class="post post__${this.id}">
				<div class="post__button-container-${this.id}"></div>
				<div class="post__user">
					<div class="post__img"></div>				
					<h3 class="user__name">${this.name}</h3>
					<a href="mailto:${this.mail}" class="user__mail">${this.mail}</a>
				</div>					
				<h2 class="post__title post__title-${this.id}">${this.title}</h2>
				<p class="post__text post__text-${this.id}">${this.body}</p>
   			</article>`)
		const buttonContainer = document.querySelector(`.post__button-container-${this.id}`)
		buttonContainer.append(this.deleteButton)
		this.deleteButton.classList.add("post__delete");
		buttonContainer.append(this.editButton)
		this.editButton.classList.add("post__edit");
		this.editButton.innerHTML = "edit";
	}

	render() {
		this.createElements();
		this.editButton.addEventListener("click", () => { editPost(this.title, this.body, this.id) });
		this.deleteButton.addEventListener("click", () => { deletePost(this.id) });
	};
}

const editPost = (title, body, id) => {
	const enteredEditTitle = prompt("Edit title:", title);
	const enteredEditBody = prompt("Edit body:", body)

	if (enteredEditTitle !== null || enteredEditBody !== null) {
		const titleContainer = document.querySelector(`.post__title-${id}`)
		titleContainer.innerHTML = enteredEditTitle;
		const bodyContainer = document.querySelector(`.post__text-${id}`)
		bodyContainer.innerHTML = enteredEditBody;

		fetch(`https://ajax.test-danit.com/api/json/posts/${id}`, {
			method: 'PUT',
			body: JSON.stringify({
				title: enteredEditTitle,
				body: enteredEditBody
			}),
			headers: {
				'Content-Type': 'application/json'
			}
		})
			.then(response => response.json())
			.catch((err) => console.warn(err))
	}
}

const deletePost = (id) => {
	fetch(`https://ajax.test-danit.com/api/json/posts/${id}`, {
		method: "DELETE",
	}).then(() => {
		container.removeChild(document.querySelector(`.post__${id}`));
	}).catch((err) => console.warn(err)
	)
}

const createPost = () => {
	const createPostBtn = document.createElement('div');
	createPostBtn.classList.add('create-new-post');
	createPostBtn.innerHTML = "Create new post";
	document.body.append(createPostBtn);

	createPostBtn.addEventListener("click", () => {
		const enteredTitle = prompt("Entered title:", "title");
		const enteredBody = prompt("Entered body title:", "body")
		fetch("https://ajax.test-danit.com/api/json/posts", {
			method: 'POST',
			body: JSON.stringify({
				userId: 1,
				title: `${enteredTitle}`,
				body: `${enteredBody}`
			}),
			headers: {
				'Content-Type': 'application/json'
			}
		})
			.then(response => response.json())
			.then(({ body, title, userId, id }) => {
				console.log(id);
				if (enteredTitle !== null || enteredBody !== null) {
					new Post(body, title, userId, "Leanne Graham", "Sincere@april.biz", id).render();
				}
			})
			.catch((err) => console.warn(err))
	})
}

const startLoader = () => {
	const loader = document.createElement('div');
	loader.classList.add('middle');
	loader.innerHTML = `<div class="bar bar1"></div>
				<div class="bar bar2"></div>
				<div class="bar bar3"></div>
				<div class="bar bar4"></div>
				<div class="bar bar5"></div>
				<div class="bar bar6"></div>
				<div class="bar bar7"></div>
				<div class="bar bar8"></div>`;
	document.body.append(loader);
}

let postsArray = [];

const getAllPosts = () => {
	startLoader();
	fetch("https://ajax.test-danit.com/api/json/posts")
		.then((response) => response.json())
		.then((json) =>
			json.forEach(({ body, title, userId, id }) =>
				fetch("https://ajax.test-danit.com/api/json/users")
					.then((res) => res.json())
					.then((data) => {
						data.forEach(({ name, email, id: idUser }) => {
							if (idUser === userId) {
								postsArray.push({ body, title, userId, name, email, id });
								// new Post(body, title, userId, name, email, id).render();
							}
						})
					})
					.catch((err) => console.warn(err))
			)
		)
		.catch((err) => console.warn(err));
}

const drawUsers = () => {
	setTimeout(() => {
		document.querySelector(".middle").remove();
		createPost();
		postsArray.forEach(({ body, title, userId, name, email, id }) => {
			new Post(body, title, userId, name, email, id).render()
		})
	}, 6000);
}

getAllPosts();
drawUsers();