"use strict"

// Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript:
//  - це можливість мови JS повторно використовувати те, що має батьківський об'єкт, не копіювати його методи, а просто створити новий об'єкт на його основі.

// Для чого потрібно викликати super() у конструкторі класу-нащадка? :
//  - запозичує конструктор у класу батька після чого можемо його доповнити!



class Employee {
	constructor(name, age, salary) {
		this._name = name;
		this._age = age;
		this._salary = salary;
	}

	get name() {
		return this._name;
	}
    set name(value) {
		this._name = value;
	}

	get age() {
		return this._age;
	}
    set age(value) {
		this._age = value;
	}

	get salary() {
		return this._salary;
	}
	set salary(value) {
		this._salary = value;
	}
}

class Programmer extends Employee {
	constructor(name, age, salary, lang) {
		super(name, age, salary);
		this._lang = lang;
	}

	get salary() {
		return super.salary * 3;
	}
}

const Bob = new Programmer('Bob', 26, 3000, 'JS');
const Den = new Programmer('Den', 36, 2700, 'C++');
const Garry = new Programmer('Garry', 20, 1200, 'Java');

console.log(Bob);
console.log(Bob.salary);
console.log(Den);
console.log(Den.salary);
console.log(Garry);
console.log(Garry.salary);
