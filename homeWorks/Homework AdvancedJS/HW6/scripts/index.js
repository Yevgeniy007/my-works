// JavaScript - це однопоточна мова програмування, в якій код виконується в одному стеку викликів.

// JavaScript дозволяє виконувати асинхронний код. Але асинхронність у JavaScript не означає те саме, що однопоточність чи багато потоків.

// JavaScript може мати асинхронний код, але він як правило однопотоковий.


"use strict";

const serchIdButton = document.querySelector(".serch-id");

serchIdButton.addEventListener("click", async () => {
	try {
		const { ip } = await fetch("https://api.ipify.org/?format=json")
			.then((response) => response.json())

		const { status, continent, city, country, regionName, district } = await fetch(
			`http://ip-api.com/json/${ip}?fields=status,message,continent,continentCode,country,countryCode,region,regionName,city,district,zip,lat,lon,timezone,offset,currency,isp,org,as,asname,reverse,mobile,proxy,hosting,query`
		).then((res) => res.json())

		if ( status === "success") {
			document.querySelector(".user-info").innerHTML = 
			`<p class="user-info__item">Континент: ${continent}</p>
    		<p class="user-info__item">Країна: ${country}</p>
    		<p class="user-info__item">Регіон: ${regionName}</p>
    		<p class="user-info__item">Місто: ${city}</p>
    		<p class="user-info__item">Район: ${district}</p>`;
		}
	} catch (error) {
		console.warn(error);
	}
});
