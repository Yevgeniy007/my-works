"use strict"

//Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.:
//  - коли ми беремо дані з інших джерел і не впевнені в їх коректності - використовуємо try...catch. і цим не даємо зупинити весь код
//  - для виконання якихось дій при введенні некоректних даних користувачем та попереджувати його про це!

const books = [
	{ 
	  author: "Люсі Фолі",
	  name: "Список запрошених",
	  price: 70 
	}, 
	{
	 author: "Сюзанна Кларк",
	 name: "Джонатан Стрейндж і м-р Норрелл",
	}, 
	{ 
	  name: "Дизайн. Книга для недизайнерів.",
	  price: 70
	}, 
	{ 
	  author: "Алан Мур",
	  name: "Неономікон",
	  price: 70
	}, 
	{
	 author: "Террі Пратчетт",
	 name: "Рухомі картинки",
	 price: 40
	},
	{
	 author: "Анґус Гайленд",
	 name: "Коти в мистецтві",
	}
  ];

class NotPropertyError extends Error {
    constructor(propName) {
        super();
        this.name = "NotProperty";
        this.message = `Object has no property: ${propName}`;
    }
}

class Book {
    constructor(obj) {
        if (!(obj.hasOwnProperty("author"))) {
            throw new NotPropertyError("author");
        }
		if (!(obj.hasOwnProperty("name"))) {
			throw new NotPropertyError("name");
		} 
		if (!(obj.hasOwnProperty("price"))) {
			throw new NotPropertyError("price");
		} 
        this.obj = obj;
    }

    render(container) {
        container.insertAdjacentHTML('beforeend', `
        <li class="item" >
			<div class = "item-div">
            	<p>${this.obj.author}</p>
				<p>${this.obj.name}</p>
				<p>${this.obj.price}</p>
			</div>
        </li>`)
    }
}

const container = document.createElement("ul");
container.classList.add("grid")
const divRoot = document.querySelector("#root")
divRoot.append(container)

books.forEach((el) => {
    try {
        new Book(el).render(container);
    } catch (err) {
        if (err.name === "NotProperty") {
            console.warn(err);
        } else {
            throw err;
        }
    }
})